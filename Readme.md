# helm-org-clock-select-task #

This package provides a replacement for `org-clock-select-task` from org-clock.el.

Differences comparing to `org-clock-select-task`:
* no limit on number of saved tasks (`org-clock-select-task` is limited to 35
  tasks because of alphabet length)
* uses [helm][1] for completion and selection, enabling multiple commands, not
  just clocking in
* instead of remembering buffer positions, uses `ID` property for tracking
  tasks, which is more reliable, as buffer positions shift constantly when
  file is changing

## Installation ##

## Usage ##

```lisp
(with-eval-after-load 'org-clock
  (require 'helm-org-clock-select-task)
  (helm-org-clock-select-task-setup))
```

`M-x helm-org-clock-select-task` (or bind it somewhere).

[1]: https://emacs-helm.github.io/helm
