;;; helm-org-clock-select-task.el --- Replacement for `org-clock-select-task' using Helm -*- lexical-binding: t -*-

;; Copyright (C) 2016-2021 Victor Deryagin

;; Author: Victor Deryagin <vderyagin@gmail.com>
;; Maintainer: Victor Deryagin <vderyagin@gmail.com>
;; Created: 13 Mar 2016
;; Version: 0.3.0

;; Package-Requires: ((helm))

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;;; Code:

(require 'helm)
(require 'org-clock)
(require 'org-id)
(require 'seq)
(require 'subr-x)

(defgroup helm-org-clock-select-task nil
  "Replacement for `org-clock-helm-select-task' using Helm"
  :prefix "hocst/"
  :group 'helm)

(defcustom helm-org-clock-select-task-id-file
  (expand-file-name "helm-org-clock-select-task-ids" user-emacs-directory)
  "File to save IDs of clocked tasks in.'"
  :group 'helm-org-clock-select-task
  :type 'file)

(defvar hocst/ids nil)
(defvar hocst/loaded nil)

(defvar helm-org-clock-select-task-keymap
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map helm-map)
    (define-key map (kbd "C-O")
      (lambda ()
        (interactive)
        (helm-run-after-exit #'org-clock-out)
        (helm-force-update)))
    (define-key map (kbd "C-X")
      (lambda ()
        (interactive)
        (call-interactively #'org-clock-cancel)
        (helm-force-update)))
    (define-key map (kbd "C-D")
      (lambda ()
        (interactive)
        (hocst/cleanup-ids (helm-marked-candidates))
        (helm-force-update)))
    map))

(defun hocst/load-saved-ids ()
  (unless hocst/loaded
    (setq hocst/ids nil)

    (when (file-exists-p helm-org-clock-select-task-id-file)
      (condition-case nil
          (with-temp-buffer
            (insert-file-contents-literally helm-org-clock-select-task-id-file)
            (setq hocst/ids (split-string (buffer-string) "\n" 'omit-nulls)
                  hocst/loaded t))
        (error
         (message "Failed to read from %s" helm-org-clock-select-task-id-file))))))

(defun hocst/save-ids ()
  (when (or hocst/loaded
            (not (file-exists-p helm-org-clock-select-task-id-file)))
    (hocst/cleanup-ids)
    (with-temp-file helm-org-clock-select-task-id-file
      (insert (mapconcat #'identity hocst/ids "\n")))))

(defun hocst/cleanup-ids (&optional ids-to-remove)
  "Remove invalid ids from `hocst/ids'. Also remove IDS-TO-REMOVE, if specified."
  (setq hocst/ids
        (seq-filter (lambda (id)
                      (and (not (member id ids-to-remove))
                           (hocst/valid-p id)))
                    hocst/ids)))

(defun hocst/valid-p (id)
  "Check if ID is of an existing, non-done heading."
  (when-let (marker (hocst/id->marker id))
    (org-with-point-at marker
      (and (org-at-heading-p)
           (not (org-entry-is-done-p))))))

(defun hocst/id->marker (id)
  (org-id-find id 'marker))

(defun hocst/marker->id (marker)
  (when (and (markerp marker)
             (marker-buffer marker))
    (org-with-point-at marker
      (org-id-get-create))))

(defun hocst/goto-id (id)
  (org-goto-marker-or-bmk (hocst/id->marker id)))

(defun hocst/get-heading (id)
  (org-with-point-at (hocst/id->marker id)
    ;; code borrowed from `org-clock-insert-selection-line' function in org-clock.el
    (let ((heading (org-get-heading))
          (prefix (progn (looking-at org-outline-regexp)
                         (match-string 0))))
      (substring (org-fontify-like-in-org-mode (concat prefix heading)
                                               org-odd-levels-only)
                 (length prefix)))))

(defmacro hocst/action-on-id (func)
  `(lambda (id)
     (org-with-point-at (hocst/id->marker id)
       (call-interactively ,func))))

(defclass hocst/source (helm-source)
  ((candidate-transformer
    :initform
    (apply-partially #'seq-map (lambda (id) (cons (hocst/get-heading id) id))))
   (action
    :initform
    (list (cons "Clock in"
                (hocst/action-on-id #'org-clock-in))
          (cons "Go to heading"
                #'hocst/goto-id)))
   (mode-line
    :initform
    (list "task(s)"
          (mapconcat
           #'identity
           '("RET:Clock in"
             "F2:Go to heading"
             "C-O:Clock out"
             "C-X:Cancel clock"
             "C-D:Remove from history")
           " ")))
   (keymap
    :initform
    'helm-org-clock-select-task-keymap)
   (persistent-action
    :initform
    #'hocst/goto-id)
   (persistent-help
    :initform
    "Show heading")))

(defclass hocst/current-task-source (hocst/source)
  ((action
    :initform
    (list (cons "Go to heading"
                #'hocst/goto-id)
          (cons "Clock out"
                (hocst/action-on-id #'org-clock-out))
          (cons "Cancel clock"
                (hocst/action-on-id #'org-clock-cancel))
          (cons "Change TODO keyword"
                (hocst/action-on-id #'org-todo))))
   (keymap
    :initform
    'helm-map)
   (mode-line
    :initform
    (list "task"
          (mapconcat
           #'identity
           '("RET:Go to heading"
             "F2:Clock out"
             "F3:Cancel clock"
             "F4:Change TODO keyword")
           " ")))))

(defun hocst/clock-in-function ()
  "Make sure that every clocked task has an ID, and that ID is in `hocst/ids'."
  (hocst/load-saved-ids)
  (add-to-list 'hocst/ids (org-id-get-create)))

;;;###autoload
(defun helm-org-clock-select-task-setup ()
  (add-hook 'org-clock-in-hook #'hocst/clock-in-function)
  (add-hook 'kill-emacs-hook #'hocst/save-ids))

;;;###autoload
(defun helm-org-clock-select-task ()
  (interactive)

  (unless org-clock-loaded
    (org-clock-load))

  (hocst/load-saved-ids)

  (hocst/cleanup-ids)

  (let* ((current-task-id (hocst/marker->id org-clock-marker))
         (default-task-id (hocst/marker->id org-clock-default-task))
         (interrupted-task-id (hocst/marker->id org-clock-interrupted-task))
         (recent-tasks-ids (seq-remove (lambda (id)
                                         (member id (list current-task-id
                                                          default-task-id
                                                          interrupted-task-id)))
                                       hocst/ids))
         (sources
          (list
           (helm-make-source "Current task" #'hocst/current-task-source
             :candidates (and current-task-id (list current-task-id)))
           (helm-make-source "Default task" #'hocst/source
             :candidates (and default-task-id (list default-task-id)))
           (helm-make-source "Interrupted task" #'hocst/source
             :candidates (and interrupted-task-id (list interrupted-task-id)))
           (helm-make-source "Recently clocked tasks" #'hocst/source
             :candidates recent-tasks-ids))))
    (helm :buffer  " *helm org clock select task*"
          :prompt  "Task: "
          :sources sources)))

(provide 'helm-org-clock-select-task)

;;; helm-org-clock-select-task.el ends here
